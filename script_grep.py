#!/usr/bin/env python3

##########################################
##  IMPORT                              ##
##########################################

import json
import sys

##########################################
##  FUNCTIONS                           ##
##########################################

def greppe(filename):
    with open(filename, 'r') as fd:
        buf = fd.read()
        values = json.loads(buf)

    for v in values:
        isin = v['isin']
        name = v['name']
        curr = v['currency'] if 'currency' in v.keys() else '---'
        print(isin, curr, name)



##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("%s <file>" % sys.argv[0])
    else:
        for filename in sys.argv[1:]:
            greppe(filename)
