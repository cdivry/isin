#!/usr/bin/env python3

##########################################
##  IMPORT                              ##
##########################################

import json
import sys

##########################################
##  FUNCTIONS                           ##
##########################################

def remove_doublons(filename):
    values = []
    with open(filename, 'r') as fd:
        buf = fd.read()
        values = json.loads(buf)

    final = []

    for e in values:
        if e not in final:
             final.append(e)

    final = sorted(final, key=lambda k: k['page'] if 'page' in k else k['isin'], reverse=False)
    print('Doublons:', '\n', len(final), "unique(s) item(s) /", len(values))
    out = json.dumps(final, sort_keys=True, indent=4)

    # final write
    with open(filename, 'w') as fd:
        fd.write(out)
        print("File '%s' written." % filename)





##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("%s <file>" % sys.argv[0])
    else:
        for filename in sys.argv[1:]:
            remove_doublons(filename)
