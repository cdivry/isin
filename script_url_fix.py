#!/usr/bin/env python3

##########################################
##  IMPORT                              ##
##########################################

import json
import sys

##########################################
##  FUNCTIONS                           ##
##########################################

def url_fix(filename):
    with open(filename, 'r') as fd:
        buf = fd.read()
        values = json.loads(buf)

        final = []

        for e in values:
            if 'page' in e and e['page'] and e['page'][0:26] == 'https://www.boursorama.com':
                 e['page'] = e['page'].replace('https://www.boursorama.com', '')
            if e not in final:
                 final.append(e)

        #out = json.dumps(final)
        out = json.dumps(final, sort_keys=True, indent=4)

    # final write
    with open(filename, 'w') as fd:
        fd.write(out)
        print("URLs for file '%s' are fixed." % filename)





##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("%s <file>" % sys.argv[0])
    else:
        for filename in sys.argv[1:]:
            url_fix(filename)
