#!/usr/bin/env python3

##########################################
##  IMPORT                              ##
##########################################

import json
import sys
import os

##########################################
##  FUNCTIONS                           ##
##########################################

def get_prefix(isin):
    if isin:
        _tmp = ""
        if ord(isin[0]) >= ord('0') and ord(isin[0]) <= ord('9'):
            return (isin[0:3])
        for ch in isin:
            if ord(ch) >= ord('0') and ord(ch) <= ord('9'):
                return (_tmp)
            _tmp += ch
    return(None)

def html_replace_scripts(html):
    with open('./html/js/Chart.min.js', 'r') as fd:
        script = fd.read()
        html = html.replace('<script src="js/Chart.min.js"></script>', '<script>' + script + '</script>')
    with open('./html/js/utils.js', 'r') as fd:
        script = fd.read()
        html = html.replace('<script src="js/utils.js"></script>', '<script>' + script + '</script>')
    return (html)

def charts_prefixes(filename):

    values = []
    with open(filename, 'r') as fd:
        buf = fd.read()
        values = json.loads(buf)

    prefixes = []
    prefixes_diagram = {}

    for e in values:

        pfx = get_prefix(e['isin'])
        if pfx:

            if pfx not in prefixes:
                prefixes.append(pfx)

            if pfx not in prefixes_diagram:
                prefixes_diagram[ pfx ] = 0

            prefixes_diagram[pfx] += 1

    # sort
    prefixes.sort()
    prefixes_diagram = sorted(prefixes_diagram.items(), key=lambda kv: kv[1], reverse=True)

    # quick output
    for pfx,amount in prefixes_diagram:
        print(pfx, amount)

    # html output
    market = filename.split('/')[::-1][0].replace('.json', '')

    if not os.path.isdir('./html/prefix/'):
        os.mkdir('./html/prefix/')

    html_file = './html/prefix/prefix_' + market + '.html'
    html = ""
    with open('./html/template.html', 'r') as fd:
        buf = fd.read()
        html = buf % {
	    'TITLE': "Répartition des préfixes ISIN sur le marché %s." % market,
            'DATA':   [ v for k,v in prefixes_diagram ],
            'LABELS': [ k for k,v in prefixes_diagram ],
        }
        html = html_replace_scripts(html)
    with open(html_file, 'w') as fd:
        fd.write(html)
    print("HTML file '%s' written." % html_file)








##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("%s <file>" % sys.argv[0])
    else:
        for filename in sys.argv[1:]:
            charts_prefixes(filename)
