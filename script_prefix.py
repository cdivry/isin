#!/usr/bin/env python3

##########################################
##  IMPORT                              ##
##########################################

import json
import sys
import os

##########################################
##  FUNCTIONS                           ##
##########################################

def get_prefix(isin):
    if isin:
        _tmp = ""
        if ord(isin[0]) >= ord('0') and ord(isin[0]) <= ord('9'):
            return (isin[0:3])
        for ch in isin:
            if ord(ch) >= ord('0') and ord(ch) <= ord('9'):
                return (_tmp)
            _tmp += ch
    return(None)

def get_all_prefixes(filename, prefixes={}):

    with open(filename, 'r') as fd:
        buf = fd.read()
        values = json.loads(buf)

    for v in values:
        isin = v['isin']
        prfx = get_prefix(v['isin'])
        if prfx and prfx not in prefixes.keys():
            prefixes[prfx] = []
        if isin and isin not in prefixes[prfx]:
            prefixes[prfx].append(v)

    return (prefixes)


##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("%s <file>" % sys.argv[0])
    else:
        prfxs = {}
        for filename in sys.argv[1:]:
            prfxs = get_all_prefixes(filename, prfxs)

        for pfx in prfxs.keys():
            out_file = './data/prefix/prefix_' + pfx + '.json'

            if not os.path.isdir('./data/prefix/'):
                os.mkdir('./data/prefix/')

            if os.path.isfile(out_file):
                with open(out_file, 'r') as fd:
                    buf = fd.read()
                    data = json.loads(buf)
            else:
                data = []

            for elem in prfxs[pfx]:
                if elem not in data:
                    data.append(elem)

            print(len(data), pfx)
            with open(out_file, 'w') as fd:
                fd.write(json.dumps(data))
                print("JSON out file '%s' has been written" % out_file)
