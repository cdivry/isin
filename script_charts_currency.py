#!/usr/bin/env python3

##########################################
##  IMPORT                              ##
##########################################

import json
import sys
import os

##########################################
##  FUNCTIONS                           ##
##########################################

def remove_digits(str):
    return (''.join([i for i in str if not i.isdigit()]))

def html_replace_scripts(html):
    with open('./html/js/Chart.min.js', 'r') as fd:
        script = fd.read()
        html = html.replace('<script src="js/Chart.min.js"></script>', '<script>' + script + '</script>')
    with open('./html/js/utils.js', 'r') as fd:
        script = fd.read()
        html = html.replace('<script src="js/utils.js"></script>', '<script>' + script + '</script>')
    return (html)

def charts_currencies(filename):

    values = []
    with open(filename, 'r') as fd:
        buf = fd.read()
        values = json.loads(buf)

    currencies = []
    currency_diagram = {}

    for e in values:

        if 'currency' not in e:
            e['currency'] = None

        if e['currency']:
            e['currency'] = e['currency'].strip()

            if e['currency'] not in currencies:
                currencies.append(e['currency'])

            if e['currency'] not in currency_diagram:
                currency_diagram[ e['currency'] ] = 0

            currency_diagram[e['currency']] += 1


    # sort
    currencies.sort()
    currency_diagram = sorted(currency_diagram.items(), key=lambda kv: kv[1], reverse=True)

    # quick output
    for curr,amount in currency_diagram:
        print(curr, amount)

    if not os.path.isdir('./html/currency/'):
        os.mkdir('./html/currency/')


    # html output
    market = filename.split('/')[::-1][0].replace('.json', '')
    html_file = './html/currency/currency_' + market + '.html'
    html = ""
    with open('./html/template.html', 'r') as fd:
        buf = fd.read()
        html = buf % {
	    'TITLE': "Répartition des devises sur le marché %s" % market,
            'DATA':   [ v for k,v in currency_diagram ],
            'LABELS': [ k for k,v in currency_diagram ],
        }
        html = html_replace_scripts(html)
    with open(html_file, 'w') as fd:
        fd.write(html)
    print("HTML file '%s' written." % html_file)








##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("%s <file>" % sys.argv[0])
    else:
        for filename in sys.argv[1:]:
            charts_currencies(filename)
