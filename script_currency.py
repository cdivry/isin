#!/usr/bin/env python3

##########################################
##  IMPORT                              ##
##########################################

import json
import sys
import os

##########################################
##  FUNCTIONS                           ##
##########################################

def get_all_currencies(filename, currencies={}):

    with open(filename, 'r') as fd:
        buf = fd.read()
        values = json.loads(buf)

    for v in values:
        isin = v['isin'] if 'isin' in v.keys() else None
        curr = v['currency'] if 'currency' in v.keys() else None
        if curr and curr not in currencies.keys():
            currencies[curr] = []
        if curr:
            currencies[curr].append(v)

    return (currencies)


##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("%s <file>" % sys.argv[0])
    else:
        currencies = {}
        for filename in sys.argv[1:]:
            currencies = get_all_currencies(filename, currencies)

        for curr in currencies.keys():
            out_file = './data/currency/currency_' + curr + '.json'

            if not os.path.isdir('./data/currency/'):
                os.mkdir('./data/currency/')

            if os.path.isfile(out_file):
                with open(out_file, 'r') as fd:
                    buf = fd.read()
                    data = json.loads(buf)
            else:
                data = []

            for elem in currencies[curr]:
                if elem not in data:
                    data.append(elem)

            print(len(data), curr)
            with open(out_file, 'w') as fd:
                fd.write(json.dumps(data))
                print("JSON out file '%s' has been written" % out_file)
