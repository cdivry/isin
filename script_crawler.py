#!/usr/bin/env python3

##########################################
##  IMPORT                              ##
##########################################

import requests
import bs4
import time
import datetime
import json

##########################################
##  FUNCTIONS                           ##
##########################################

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return (func)
    return (decorate)

def log_line(msg):
    date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    line = '[' + date + '] ' + msg
    print(line)
    with open('logs.txt', 'a') as fd:
        fd.write(line + '\n')

def bourso_page_links(url):
    links = []
    html = ""
    try:
        res = requests.get(url)
        if res.status_code == 200:
            html = bs4.BeautifulSoup(res.text, features='html.parser')
    except:
        log_line(url)
        log_line('ERROR:', "bourso_page_links(), retrying in 5 sec...")
        time.sleep(5)
        return (bourso_page_links(url))

    if html != "":
        table = html.find('div', {'class': 'c-palmares'})
        links = table.findAll('a', {'class': 'c-link--animated'})
    return (links)

@static_vars(tmp={})
def bourso_infos(url):
    if url[0:7] == "/cours/":
        url = 'https://www.boursorama.com' + url

    if url not in bourso_infos.tmp.keys():
        name, code, isin, curr, elig = (None, None, None, None, None)

        try:
            res = requests.get(url)
        except:
            log_line("ERROR:", "bourso_infos() connection failed, retrying in 5 sec...")
            time.sleep(5)
            return (bourso_infos(url))

        if res.status_code == 200:
            html = bs4.BeautifulSoup(res.text, features="html.parser")
            name = html.title.text.split('Cours')[0]
            if html.find('h2', {'class':'c-faceplate__isin'}):
                isin = html.find('h2', {'class':'c-faceplate__isin'}).text.split(' ')[0]
                code = html.find('h2', {'class':'c-faceplate__isin'}).text.split(' ')[1]
                curr = html.find('span', {'class':'c-faceplate__price-currency'}).text
            else:
                log_line("ERROR: URL incorrecte '%s'" % url)
        else:
            log_line("ERROR: HTTP %d for page '%s'" % (res.status_code, url))
        bourso_infos.tmp[url] = (name, code, isin, curr, elig)
    else:
        log_line('--- ' + bourso_infos.tmp[url][3] + ' ' + bourso_infos.tmp[url][0] + ' (already in memory)')

    return (bourso_infos.tmp[url])

def crawl_page_url(indice, page):

    # France
    if indice == 'Paris':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/france/page-{PAGE}?france_filter%5Bmarket%5D=TOUS&france_filter%5Bsector%5D=&france_filter%5Bvariation%5D=50003&france_filter%5Bperiod%5D=3&france_filter%5Bfilter%5D='

    # 31 Pays-Bas
    elif indice == 'Amsterdam':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=31&international_filter%5BindexTrading%5D=3101&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'AEX25':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=31&international_filter%5BindexTrading%5D=1rAAEX&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'AMX':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=31&international_filter%5BindexTrading%5D=1rAAMX&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    # 32 Belgique
    elif indice == 'Bruxelles':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=32&international_filter%5BindexTrading%5D=3201&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'BEL20':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=32&international_filter%5BindexTrading%5D=FF11-BEL20&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='


    # 34 Espagne
    elif indice == 'Sibe':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=34&international_filter%5BindexTrading%5D=3401&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'IBEX35':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=34&international_filter%5BindexTrading%5D=FF55-IBEX&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'Latibex':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=34&international_filter%5BindexTrading%5D=FF55-B&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    # 35 Portugal
    elif indice == 'Lisbonne':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=35&international_filter%5BindexTrading%5D=3501&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'PSI20':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=35&international_filter%5BindexTrading%5D=1rLPSI20&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    # 39 Italie
    elif indice == 'Milan':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=39&international_filter%5BindexTrading%5D=3901&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'FTSE-MIB':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=39&international_filter%5BindexTrading%5D=7fI945&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    # 41 Suisse
    elif indice == 'Zurich':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=41&international_filter%5BindexTrading%5D=4101&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'VirtX':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=41&international_filter%5BindexTrading%5D=4104&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'SMI25':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=41&international_filter%5BindexTrading%5D=1hSMI&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    # 44 Royaume Uni
    elif indice == 'LSE':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=44&international_filter%5BindexTrading%5D=4402&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'FTSE250':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=44&international_filter%5BindexTrading%5D=3uMCX.L&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'Footsie100':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=44&international_filter%5BindexTrading%5D=UKX.L&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    # 49 Allemagne
    elif indice == 'Xetra':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=49&international_filter%5BindexTrading%5D=4920&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'DAX30':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=49&international_filter%5BindexTrading%5D=5pDAX&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'MDAX':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=49&international_filter%5BindexTrading%5D=5pMDAX&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'SDAX':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=49&international_filter%5BindexTrading%5D=5pSDXP&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    elif indice == 'TecDAX':
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=49&international_filter%5BindexTrading%5D=5pTDXP&international_filter%5Bvariation%5D=50003&international_filter%5Bperiod%5D=3&international_filter%5Bfilter%5D='

    # EU Europe
    else:
        tmp_url = 'https://www.boursorama.com/bourse/actions/palmares/international/page-{PAGE}?international_filter%5Bcountry%5D=EU&international_filter%5Bperiod%5D=1'

    if page >= 2:
        url = tmp_url.replace('{PAGE}', str(page))
    else:
        url = tmp_url.replace('page-{PAGE}', '')
    return (url)

@static_vars(trash=[])
def crawl_page(page, indice, index=0, trash=None):
    log_line("MARKET " +indice + " PAGE " +  str(page))
    url = crawl_page_url(indice, page)

    if index != 0:
        log_line('Resuming page %d, index %d' % (page, index))
    if trash != None:
        crawl_page.trash = trash
    values = []

    links = bourso_page_links(url)

    log_line(str(len(links)) + ' valeur(s) à analyser')
    for a in links[index:]:
        if a.attrs['href'] not in crawl_page.trash and a.attrs['href'][0:7] == "/cours/":
            elem = {
                'name': a.text,
                'code': a.attrs['href'].split('/')[2],
                'isin': '',
                'page': a.attrs['href'],
            }
            name, code, isin, curr, elig = bourso_infos(elem['page'])
            if code:
                _pos = elem['code'].find(code)
                elem['prefix'] = elem['code'][0:_pos]
                elem['code'] = elem['code'].replace(elem['prefix'], '')
            elem['isin'] = isin
            elem['eligible'] = elig
            if curr:
                elem['currency'] = curr.strip()
            values.append(elem)
            crawl_page.trash.append(a.attrs['href'])
            log_line('+++ +' + str(len(values)) + ' ' + elem['isin'] + ' ' + elem['name'])

        elif a.attrs['href'][0:7] == "/cours/":
            log_line('--- ' + a.attrs['href'] + ' (already crawled)')

        index += 1
    return (values)



def crawl_indice(indice, max_pages=77):
    values = load_sometimes(indice)
    trash = [ e['page'] for e in values ]
    log_line('#####################')
    log_line('INDICE ' +  indice)
    log_line('VALUES ' + str(len(values)))
    log_line('TRASH ' + str(len(trash)))
    for page in range(1, max_pages + 1):
        values += crawl_page(page, indice, trash=trash)
        save_sometimes(values, indice, full=True)
        values = load_sometimes(indice)


def save_sometimes(values, indice, full=False):
    now = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    if full:
        log_line("Saving into ./data/" + indice + '.json (full)')
        with open('./data/' + indice + '.json', 'w') as fd:
            fd.write(json.dumps(values))
    else:
        with open('./data/' + indice + '_' + now + '_TMP.json', 'w') as fd:
            fd.write(json.dumps(values))

    end = '(tmp)' if not full else '(full)'
    log_line(str(len(values)) + ' element(s) (re)saved. ' + end)

def load_sometimes(indice):
    try:
        with open('./data/' + indice + '.json', 'r') as fd:
            buf = fd.read()
            values = json.loads(buf)
    except:
        values = []
    log_line(str(len(values)) + ' element(s) (re)loaded.')
    return (values)




##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':

    # France
    crawl_indice('Paris', max_pages=27)

    # Royaume Uni
    crawl_indice('LSE', max_pages=77)
    crawl_indice('FTSE250', max_pages=10)
    crawl_indice('Footsie100', max_pages=5)

    # Suisse
    crawl_indice('Zurich', max_pages=10)
    crawl_indice('VirtX', max_pages=2)
    crawl_indice('SMI25', max_pages=1)

    # Espagne
    crawl_indice('Sibe', max_pages=10)
    crawl_indice('IBEX35', max_pages=2)
    crawl_indice('Latibex', max_pages=1)

    # Italie
    crawl_indice('Milan', max_pages=20)
    crawl_indice('FTSE-MIB', max_pages=10)

    # Belgique
    crawl_indice('Bruxelles', max_pages=7)
    crawl_indice('BEL20', max_pages=1)

    # Allemagne
    crawl_indice('Xetra', max_pages=50)
    crawl_indice('DAX30', max_pages=2)
    crawl_indice('MDAX', max_pages=3)
    crawl_indice('SDAX', max_pages=4)
    crawl_indice('TecDAX', max_pages=2)

    # Pays-Bas
    crawl_indice('Amsterdam', max_pages=10)
    crawl_indice('AEX25', max_pages=2)
    crawl_indice('AMX', max_pages=2)

    # Portugal
    crawl_indice('Lisbonne', max_pages=10)
    crawl_indice('PSI20', max_pages=5)

    #crawl_indice('Europe')



