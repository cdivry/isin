#!/usr/bin/env python3

##########################################
##  IMPORT                              ##
##########################################

import json
import sys

##########################################
##  FUNCTIONS                           ##
##########################################

def beautify(filename):
    with open(filename, 'r') as fd:
        buf = fd.read()
        values = json.loads(buf)

    out = json.dumps(values, sort_keys=True, indent=4)

    with open(filename, 'w') as fd:
        fd.write(out)
        print("OUT file '%s' beautified." % filename)




##########################################
##  ENTRYPOINT                          ##
##########################################

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("%s <file>" % sys.argv[0])
    else:
        for filename in sys.argv[1:]:
            beautify(filename)
